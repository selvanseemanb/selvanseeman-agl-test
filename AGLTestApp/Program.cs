﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace AGLTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var response =  ProcessGenderList();
            Console.WriteLine(Environment.NewLine + response + Environment.NewLine);

            Console.ReadKey();
        }

        static async Task<string> ProcessGenderList()
        {
            string response = string.Empty;

            try 
            { 
                using (var httpClient = new HttpClient())
                {
                    var json = await httpClient.GetStringAsync("http://agl-developer-test.azurewebsites.net/people.json");

                    // Now parse JSON deserialize to object
                    List<people> peoples = JsonSerializer.Deserialize<List<people>>(json);
                    Console.WriteLine("Total People Count: " + peoples.Count);

                    List<people> males = new List<people>();
                    List<people> females = new List<people>();
                    males.AddRange(peoples.Where(w => w.gender == "Male"));
                    females.AddRange(peoples.Where(w => w.gender == "Female"));
                    
                    Console.WriteLine(Environment.NewLine + "Male");
                    males = males.OrderBy(s => s.name).ToList();
                    foreach (people male in males)
                    {
                        Console.WriteLine("o " + male.name);
                    }
                    
                    Console.WriteLine(Environment.NewLine + "Female");
                    females = females.OrderBy(s => s.name).ToList();
                    foreach (people female in females)
                    {
                        Console.WriteLine("o " + female.name);
                    }
                }

                response = "Success, Completed!";
                // add to info log
            }
            catch (Exception ex)
            {
                response = ex.Message;
                // add to error log
            }
            finally
            {
                // clean up memeory
                // add to debug log
            }
            return "ProcessGenderList " + response;
        }
    }

    class people
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public List<pet> pets { get; set; }
    }
    class pet
    {
        public string name { get; set; }
        public string type { get; set; }
    }
}
